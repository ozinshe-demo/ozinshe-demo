//
//  MovieInfoViewController.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 17.10.2023.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import SDWebImage

class MovieInfoViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate{
   
    
    
    @IBOutlet weak var movieImage: UIImageView!
    
    @IBOutlet weak var screenshotLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var favoriteLabel: UILabel!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var producerLabel: UILabel!
    @IBOutlet weak var directorLabel: UILabel!
    @IBOutlet weak var descriptionGradientView: GradientView!
    @IBOutlet weak var yearGenreLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var arrowImageVie: UIImageView!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var screenshotCollectionView: UICollectionView!
    @IBOutlet weak var similarCollectionView: UICollectionView!
    @IBOutlet weak var similarMoviesLabel: UILabel!
    @IBOutlet weak var seasonButton: UIButton!
    @IBOutlet weak var seriesLabel: UILabel!
    @IBOutlet weak var fullDescriptionButton: UIButton!
    @IBOutlet weak var nameOfProducerLabel: UILabel!
    @IBOutlet weak var nameOfDirectorLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var directorToFullConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var screenToViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var screenToSeriesConstraint: NSLayoutConstraint!
    @IBOutlet weak var directorToDescripConstrint: NSLayoutConstraint!
        
    @IBOutlet weak var screenToSuperviewConstr: NSLayoutConstraint!
    
    @IBOutlet weak var screenToSimilarConstrain: NSLayoutConstraint!
    var movie = Movie()
    
    var similarMovies:[Movie] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setData()
        configureViews()
        downloadSimilar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        setData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
   
    
    //Changing configurations
    
    func configureViews(){
        backgroundView.layer.cornerRadius = 32.0
        backgroundView.clipsToBounds = true
        backgroundView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        descriptionLabel.numberOfLines = 4
    
        screenshotCollectionView.dataSource = self
        screenshotCollectionView.delegate = self
        
        if movie.movieType == "MOVIE"{
            seriesLabel.isHidden = true
            seasonButton.isHidden = true
            arrowImageVie.isHidden = true
            screenToViewConstraint.priority = .defaultHigh
            screenToSeriesConstraint.priority = .defaultLow
        }else{
            seasonButton.setTitle("\(movie.seasonCount) " + "SEASON".localized() + " \(movie.seriesCount) " + "EPISODE".localized(), for: .normal)
            screenToSeriesConstraint.priority = .defaultHigh
            screenToViewConstraint.priority = .defaultLow
        }
        
        if descriptionLabel.maxNumberOfLines < 5 {
            fullDescriptionButton.isHidden = true
            directorToFullConstraint.priority = .defaultLow
            directorToDescripConstrint.priority = .defaultHigh
        }
        
        if movie.favorite == true{
            favoriteButton.setImage(UIImage(named: "favButtonSelected"), for: .normal)
        }else{
            favoriteButton.setImage(UIImage(named: "favButton"), for: .normal)
        }
        
        if similarMovies.isEmpty{
            similarCollectionView.isHidden = true
            similarMoviesLabel.isHidden = true
            allButton.isHidden = true
            screenToSuperviewConstr.priority = .defaultHigh
            screenToSimilarConstrain.priority = .defaultLow
        }
    }
    
    func setData(){
        movieImage.sd_setImage(with: URL(string: movie.poster_link))
        titleLabel.text = movie.name
        
        yearGenreLabel.text = "\(movie.year)"
        
        for item in movie.genres{
            yearGenreLabel.text = yearGenreLabel.text! + " • " + item.name
        }
        
        descriptionLabel.text = movie.description
        nameOfDirectorLabel.text = movie.director
        nameOfProducerLabel.text = movie.producer
        fullDescriptionButton.setTitle("FULL".localized(), for: .normal)
        favoriteLabel.text = "ADD_TO_FAV".localized()
        shareLabel.text = "SHARE".localized()
        directorLabel.text = "DIRECTOR".localized()
        producerLabel.text = "PRODUCER".localized()
        seriesLabel.text = "SERIES".localized()
        screenshotLabel.text = "SCREENSHOTS".localized()
        similarMoviesLabel.text = "SIMILAR".localized()
        allButton.setTitle("ALL".localized(), for: .normal)
        seasonButton.setTitle("\(movie.seasonCount) " + "SEASON".localized() + " \(movie.seriesCount) " + "EPISODE".localized(), for: .normal)
        
    }
    
    func downloadSimilar(){
        SVProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(Storage.sharedInstance.accessToken)"
        ]
        
        AF.request(Urls.GET_SIMILAR + String(movie.id), method: .get, headers: headers).responseData { response in
            SVProgressHUD.dismiss()
            var resultString = ""
            if let data = response.data {
                resultString = String(data: data, encoding: .utf8)!
                print(resultString)
            }
            
            if response.response?.statusCode == 200 {
                let json = JSON(response.data!)
                print("JSON: \(json)")
                
                if let array = json.array{
                    for item in array{
                        let movie = Movie(json: item)
                        self.similarMovies.append(movie)
                    }
                    
                    self.similarCollectionView.reloadData()
                    
                } else {
                    SVProgressHUD.showError(withStatus: "CONNECTION_ERROR".localized())
                }
            } else {
                var ErrorString = "CONNECTION_ERROR".localized()
                if let sCode = response.response?.statusCode {
                    ErrorString = ErrorString + " \(sCode)"
                }
                ErrorString = ErrorString + " \(resultString)"
                SVProgressHUD.showError(withStatus: "\(ErrorString)")
            }
            
        }
    }
    
    
    @IBAction func backButtonPushed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func playButtonClicked(_ sender: Any) {
        if movie.movieType == "MOVIE"{
            let playerVC = storyboard?.instantiateViewController(withIdentifier: "MoviePlayerViewController") as! MoviePlayerViewController

            playerVC.video_link = movie.video_link
            navigationController?.show(playerVC, sender: self)
        }else{
            let seasonVC = storyboard?.instantiateViewController(withIdentifier: "SeasonsSeriesViewController") as! SeasonsSeriesViewController
            seasonVC.movie = movie

            navigationController?.show(seasonVC, sender: self)
        }
    }
    
    
    @IBAction func addFavorite(_ sender: Any) {
        var method = HTTPMethod.post
        if movie.favorite{
            method = .delete
        }
        
        SVProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(Storage.sharedInstance.accessToken)"
        ]
        
        let parameters = ["movieId": movie.id] as [String: Any]
        
        AF.request(Urls.FAVORITE_URL, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseData { response in
           
            SVProgressHUD.dismiss()
            var resultString = ""
            if let data = response.data {
                resultString = String(data: data, encoding: .utf8)!
                print(resultString)
            }
            
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
               
                self.movie.favorite.toggle()
                    
                self.configureViews()
                    
            } else {
                var ErrorString = "CONNECTION_ERROR".localized()
                if let sCode = response.response?.statusCode {
                    ErrorString = ErrorString + " \(sCode)"
                }
                ErrorString = ErrorString + " \(resultString)"
                SVProgressHUD.showError(withStatus: "\(ErrorString)")
            }
            
        }
        
    }
    
    @IBAction func shareButtonClicked(_ sender: Any) {
        let text = "\(movie.name) \n\(movie.description)"
        let image = movieImage.image
        let shareAll = [text, image!] as [Any]
        let acttivityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        acttivityViewController.popoverPresentationController?.sourceView = self.view
        self.present(acttivityViewController, animated: true)
    }
    
    @IBAction func fullDescriptionClicked(_ sender: Any) {
        if descriptionLabel.numberOfLines > 4{
            descriptionLabel.numberOfLines = 4
            fullDescriptionButton.setTitle("FULL".localized(), for: .normal)
            descriptionGradientView.isHidden = false
        }else{
            descriptionLabel.numberOfLines = 30
            fullDescriptionButton.setTitle("MINIMIZE".localized(), for: .normal)
            descriptionGradientView.isHidden = true
        }
    }
    
    //MARK: Collection Views
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.similarCollectionView{
//            if similarMovies.count == 0{
//                similarMoviesLabel.isHidden = true
//                allButton.isHidden = true
//                similarCollectionView.isHidden = true
//            }
            return similarMovies.count
        }
        return movie.screenshots.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.similarCollectionView{
            let similarMovieCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
            
            let transformer = SDImageResizingTransformer(size: CGSize(width: 112, height: 164),scaleMode: .aspectFill )
            let imageView = similarMovieCell.viewWithTag(1000) as! UIImageView
            imageView.sd_setImage(with: URL(string: similarMovies[indexPath.row].poster_link), placeholderImage: nil, context: [.imageTransformer : transformer])
            imageView.layer.cornerRadius = 8.0
            
            
            let movieName = similarMovieCell.viewWithTag(1001) as! UILabel
            movieName.text = similarMovies[indexPath.row].name
            
            let movieGenreName = similarMovieCell.viewWithTag(1002) as! UILabel
            if let genreName = similarMovies[indexPath.row].genres.first{
                movieGenreName.text = genreName.name
            }else{
                movieGenreName.text = ""
            }
            
            return similarMovieCell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        let transformer = SDImageResizingTransformer(size: CGSize(width: 184, height: 112), scaleMode: .aspectFill)
        
        let imageView = cell.viewWithTag(1000) as! UIImageView
        imageView.sd_setImage(with: URL(string: movie.screenshots[indexPath.row].link), placeholderImage: nil, context: [.imageTransformer : transformer])
        
        imageView.layer.cornerRadius = 8.0
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.similarCollectionView{
            let movieInfoVC = storyboard?.instantiateViewController(withIdentifier: "MovieInfoViewController") as! MovieInfoViewController
            movieInfoVC.movie = similarMovies[indexPath.row]
            
            navigationController?.show(movieInfoVC, sender: self)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
