//
//  MoviePlayerViewController.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 20.10.2023.
//

import UIKit

import youtube_ios_player_helper



class MoviePlayerViewController: UIViewController {
    

    @IBOutlet weak var player: YTPlayerView!
    var video_link:String = ""
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let backButton = UIBarButtonItem(image: UIImage(named: "BackButton"), style: .plain, target: self, action: #selector(backButtonClicked))
        backButton.tintColor = UIColor(named: "arrowColor")
        navigationItem.leftBarButtonItem = backButton

    }
    
    @objc func backButtonClicked(){
        navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        
        player.load(withVideoId: video_link)
        
        //        playVideo(videoIdentifier: video_link)
    }
    
    //    struct YouTubeVideoQuality {
    //        static let hd720 = NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)
    //        static let medium360 = NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)
    //        static let small240 = NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)
    //    }
    //
    //    func playVideo(videoIdentifier: String?) {
    //        let playerViewController = AVPlayerViewController()
    //
    //        self.present(playerViewController, animated: true, completion: nil)
    //
    //        XCDYouTubeClient.default().getVideoWithIdentifier(videoIdentifier) { [weak playerViewController] (video: XCDYouTubeVideo?, error: Error?) in
    //            if let streamURLs = video?.streamURLs, let streamURL = (streamURLs[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs[YouTubeVideoQuality.hd720] ?? streamURLs[YouTubeVideoQuality.medium360] ?? streamURLs[YouTubeVideoQuality.small240]) {
    //                playerViewController?.player = AVPlayer(url: streamURL)
    //            } else {
    //                self.dismiss(animated: true, completion: nil)
    //            }
    //        }
    //    }
    //
    //}
    
    
    
}
