//
//  HistoryTableViewCell.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 10.10.2023.
//

import UIKit
import SDWebImage

class HistoryTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var mainMovie = MainMovie()
    var delegate : MovieProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.dataSource = self
        collectionView.delegate = self
        
        let layout = TopAlignedCollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 0, left: 24.0, bottom: 0, right: 24.0)
        layout.estimatedItemSize.width = 156
        layout.estimatedItemSize.height = 184
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(mainMovie: MainMovie){
        
        self.mainMovie = mainMovie
        titleLabel.text = "CONTINUE_TO_WATCH".localized()
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainMovie.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        //Image
        let transformer = SDImageResizingTransformer(size: CGSize(width: 184.0, height: 112.0),scaleMode: .aspectFill)
        let imageview = cell.viewWithTag(1000) as! UIImageView
        imageview.sd_setImage(with: URL(string: mainMovie.movies[indexPath.row].poster_link),placeholderImage: nil,context: [.imageTransformer : transformer])
        imageview.layer.cornerRadius = 8.0
        
        //Name of movie
        let movieName = cell.viewWithTag(1001) as! UILabel
        movieName.text = mainMovie.movies[indexPath.row].name
        
        //Name of genre(first from genre array)
        let movieGenre = cell.viewWithTag(1002)as! UILabel
        if let genreItem =  mainMovie.movies[indexPath.row].genres.first{
            movieGenre.text = genreItem.name
        }else{
            movieGenre.text = ""
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        delegate?.movieDidSelect(movie: mainMovie.movies[indexPath.row])
    }
}


