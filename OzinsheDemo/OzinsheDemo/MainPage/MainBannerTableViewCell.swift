//
//  MainBannerTableViewCell.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 10.10.2023.
//

import UIKit
import SDWebImage

class MainBannerTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    

    @IBOutlet weak var collectionview: UICollectionView!
    
    var mainMovie = MainMovie()
    
    var delegate: MovieProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        collectionview.dataSource = self
        collectionview.delegate = self
        
        let layout = TopAlignedCollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 22, left: 24.0, bottom: 10, right: 24.0)
        layout.estimatedItemSize.width = 300
        layout.estimatedItemSize.height = 240
        layout.scrollDirection = .horizontal
        collectionview.collectionViewLayout = layout
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(mainMovie: MainMovie){
        
        self.mainMovie = mainMovie
    
        collectionview.reloadData()

        
    }
    //MARK: Collectionview
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainMovie.bannerMovie.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bannerCell", for: indexPath) as! BannerCollectionViewCell
        
        cell.setData(bannerMovie: mainMovie.bannerMovie[indexPath.row])

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        delegate?.movieDidSelect(movie: mainMovie.bannerMovie[indexPath.row].movie)
    }
    }


