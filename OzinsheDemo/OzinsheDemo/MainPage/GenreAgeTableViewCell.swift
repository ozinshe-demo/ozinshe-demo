//
//  GenreAgeTableViewCell.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 10.10.2023.
//

import UIKit
import SDWebImage

class GenreAgeTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
   

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var genreLabel: UILabel!
    
    var mainMovie = MainMovie()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        
        let layout = TopAlignedCollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 0, left: 24.0, bottom: 0, right: 24.0)
        layout.estimatedItemSize.width = 184
        layout.estimatedItemSize.height = 112
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(mainMovie: MainMovie){
        self.mainMovie = mainMovie
        if mainMovie.cellType == .ageCategory{
            genreLabel.text = "AGE_APPROPRIATE".localized()
        }else{
            genreLabel.text = "CHOOSE_GENRE".localized()
        }
        
        collectionView.reloadData()
    }
    
    //MARK: collection view
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if mainMovie.cellType == .ageCategory{
            return mainMovie.categoryAges.count
        }
        else{
            return mainMovie.genres.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        let transformer = SDImageResizingTransformer(size: CGSize(width: 184.0, height: 112.0),scaleMode: .aspectFill)
        let imageview = cell.viewWithTag(1000) as! UIImageView
        imageview.layer.cornerRadius = 8.0
        
        let movieName = cell.viewWithTag(1001) as! UILabel
       
       
        
        //Name of movie depends from cell type
        if mainMovie.cellType == .ageCategory{
            imageview.sd_setImage(with: URL(string: mainMovie.categoryAges[indexPath.row].link),placeholderImage: nil,context: [.imageTransformer : transformer])
            
            movieName.text = mainMovie.categoryAges[indexPath.row].name
            
        }else{
            imageview.sd_setImage(with: URL(string: mainMovie.genres[indexPath.row].link),placeholderImage: nil,context: [.imageTransformer : transformer])
            
            movieName.text = mainMovie.genres[indexPath.row].name
            
        }

        
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
    }

}
