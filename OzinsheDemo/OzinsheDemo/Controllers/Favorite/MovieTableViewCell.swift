//
//  MovieTableViewCell.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 21.08.2023.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var playView: UIView!
    @IBOutlet weak var watchLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        posterImageView.layer.cornerRadius = 8
        playView.layer.cornerRadius = 8
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(movie: Movie) {
        nameLabel.text = movie.name
        yearLabel.text = "\(movie.year) • \(movie.genres.first!.name) • \(movie.categories.first!.name)"
        posterImageView.sd_setImage(with: URL(string: movie.poster_link))
        watchLabel.text = "WATCH".localized()
       
        
    }

}
