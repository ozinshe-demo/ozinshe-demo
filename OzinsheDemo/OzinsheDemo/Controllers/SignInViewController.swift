//
//  SignInViewController.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 15.09.2023.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class SignInViewController: UIViewController{
    
    @IBOutlet weak var greetingsLabel: UILabel!
    
    @IBOutlet weak var accountLoginLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet weak var noAccountLabel: UILabel!
    
    @IBOutlet weak var registrationLabel: UIButton!
    @IBOutlet weak var textfieldEmail: TextFieldWithPadding!
    
    @IBOutlet weak var textfieldPassword: TextFieldWithPadding!
    
    @IBOutlet weak var signInButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureTextfields()
        hideKeyboardWhenTapped()
    }
    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        configureViews()
    }
    func languageDidChange() {
        configureViews()
    }
    func configureViews(){
        greetingsLabel.text = "HELLO".localized()
        accountLoginLabel.text = "SIGN_IN".localized()
        passwordLabel.text = "PASSWORD".localized()
        signInButton.setTitle("LOG_IN".localized(), for: .normal)
        noAccountLabel.text = "NO_ACCOUNT".localized()
        registrationLabel.setTitle("REGISTER".localized(), for: .normal)
        textfieldEmail.placeholder = "ENTER_EMAIL".localized()
        textfieldPassword.placeholder = "ENTER_PASSWORD".localized()
    }
    func configureTextfields(){
        textfieldEmail.layer.cornerRadius = 12.0
        textfieldEmail.layer.borderWidth = 1.0
        textfieldEmail.layer.borderColor = UIColor(red: 0.90, green: 0.92, blue: 0.94, alpha: 1.00).cgColor
        
        textfieldPassword.layer.cornerRadius = 12.0
        textfieldPassword.layer.borderWidth = 1.0
        textfieldPassword.layer.borderColor = UIColor(red: 0.90, green: 0.92, blue: 0.94, alpha: 1.00).cgColor
        
        signInButton.layer.cornerRadius = 12.0
        registrationLabel.titleLabel?.adjustsFontSizeToFitWidth = true
        registrationLabel.titleLabel?.minimumScaleFactor = 0.5
    }
    
    func hideKeyboardWhenTapped(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func textfieldEditDidBegin(_ sender: TextFieldWithPadding) {
        sender.layer.borderColor = UIColor(red: 0.59, green: 0.33, blue: 0.94, alpha: 1.00).cgColor
    }
    @IBAction func textfieldEditDidEnd(_ sender: TextFieldWithPadding) {
        sender.layer.borderColor = UIColor(red: 0.61, green: 0.64, blue: 0.69, alpha: 1.00).cgColor
    }
    
    @IBAction func showPassword(_ sender: Any) {
        textfieldPassword.isSecureTextEntry = !textfieldPassword.isSecureTextEntry
    }
    
    
    @IBAction func signUp(_ sender: Any) {
        
        let signUpVC = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        
        signUpVC.modalPresentationStyle = .fullScreen
        
        navigationController?.show(signUpVC, sender: self)
    }
    
    
    @IBAction func sigIn(_ sender: Any) {
        let email = textfieldEmail.text!
        let password = textfieldPassword.text!
        
        SVProgressHUD.show()
        
        let parameters = [
            "email": email,
            "password":password
        ]
        
        AF.request(Urls.SIGN_IN_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseData { response in
            SVProgressHUD.dismiss()
            var resultString = ""
            if let data = response.data {
                resultString = String(data: data, encoding: .utf8)!
                print(resultString)
            }
            
            if response.response?.statusCode == 200 {
                let json = JSON(response.data!)
                print("JSON: \(json)")
                
                if let token = json["accessToken"].string {
                    Storage.sharedInstance.accessToken = token
                    UserDefaults.standard.set(token, forKey: "accessToken")
                    self.startApp()
                } else {
                    SVProgressHUD.showError(withStatus: "CONNECTION_ERROR".localized())
                }
            } else {
                var ErrorString = "CONNECTION_ERROR".localized()
                if let sCode = response.response?.statusCode {
                    ErrorString = ErrorString + " \(sCode)"
                }
                ErrorString = ErrorString + " \(resultString)"
                SVProgressHUD.showError(withStatus: "\(ErrorString)")
            }
        }
    }
    
    func startApp(){
        let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        
        tabBarVC.modalPresentationStyle = .fullScreen
        self.present(tabBarVC, animated: true )
        
    }
    
    
    
    
    
    /*  // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}
