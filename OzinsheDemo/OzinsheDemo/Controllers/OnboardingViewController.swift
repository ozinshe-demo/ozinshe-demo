//
//  OnboardingViewController.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 13.10.2023.
//

import UIKit

class OnboardingViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{

    
    

    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var collectionView: UICollectionView!
    var arraySlides = [["firstSlide", "ONBOARD_LABEL", "ONBOARD_DESCRIPT_1"],
                       ["secondSlide", "ONBOARD_LABEL", "ONBOARD_DESCRIPT_2"],
                       ["thirdSlide", "ONBOARD_LABEL", "ONBOARD_DESCRIPT_3"]]
    
    var currentPage = 0{
        didSet{
            pageControl.currentPage = currentPage
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.title = " "
       
    }
    
    @objc func nextButtonTouched(){
        let signInController = storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        
        navigationController?.show(signInController, sender: self)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arraySlides.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        let imageview = cell.viewWithTag(1000) as! UIImageView
        imageview.image = UIImage(named: arraySlides[indexPath.row][0])
        
        let label = cell.viewWithTag(1001) as! UILabel
        label.text = arraySlides[indexPath.row][1].localized()
        
        let description = cell.viewWithTag(1002) as! UILabel
        description.text = arraySlides[indexPath.row][2].localized()
        
        let nextButton = cell.viewWithTag(1003) as! UIButton
        nextButton.layer.cornerRadius = 12.0
        nextButton.setTitle("NEXT_BUTTON".localized(), for: .normal)
        if indexPath.row != 2{
            nextButton.isHidden = true
        }
        
        nextButton.addTarget(self, action: #selector(nextButtonTouched), for: .touchUpInside)
        
        let button = cell.viewWithTag(1004) as! UIButton
        button.layer.cornerRadius = 8.0
        button.setTitle("CONTINUE_BUTTON".localized(), for: .normal)
        if indexPath.row == 2{
            button.isHidden = true
        }
        
        button.addTarget(self, action: #selector(nextButtonTouched), for: .touchUpInside)
        
        return cell
        
    
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let width = scrollView.frame.width
        currentPage = Int(scrollView.contentOffset.x / width)
    }
    
  
    

  

}
