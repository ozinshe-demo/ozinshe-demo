//
//  SignUpViewController.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 17.09.2023.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class SignUpViewController: UIViewController, LanguageProtocol{
    
    @IBOutlet weak var textfieldEmail: TextFieldWithPadding!
    
    @IBOutlet weak var textfieldPassword: TextFieldWithPadding!
    
    @IBOutlet weak var textfieldRepeatedPassword: TextFieldWithPadding!
    
    @IBOutlet weak var fillupLabel: UILabel!
    @IBOutlet weak var registrationLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordRepeatLabel: UILabel!
    @IBOutlet weak var sigInButton: UIButton!
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var registrationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureTextfields()
        hideKeyboardWhenTapped()
//        errorLabel.isHidden = true
    }
    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    func configureTextfields(){
        textfieldEmail.layer.cornerRadius = 12.0
        textfieldEmail.layer.borderWidth = 1.0
        textfieldEmail.layer.borderColor = UIColor(red: 0.90, green: 0.92, blue: 0.94, alpha: 1.00).cgColor
        
        textfieldPassword.layer.cornerRadius = 12.0
        textfieldPassword.layer.borderWidth = 1.0
        textfieldPassword.layer.borderColor = UIColor(red: 0.90, green: 0.92, blue: 0.94, alpha: 1.00).cgColor
        
        textfieldRepeatedPassword.layer.cornerRadius = 12.0
        textfieldRepeatedPassword.layer.borderWidth = 1.0
        textfieldRepeatedPassword.layer.borderColor = UIColor(red: 0.90, green: 0.92, blue: 0.94, alpha: 1.00).cgColor
        
        registrationButton.layer.cornerRadius = 12.0
        registrationButton.titleLabel?.adjustsFontSizeToFitWidth = true
        registrationButton.titleLabel?.minimumScaleFactor = 0.5
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        configureViews()
        
    }
    func languageDidChange() {
        configureViews()
    }
    func configureViews(){
        fillupLabel.text = "FILL_IN".localized()
        accountLabel.text = "HAVE_ACCOUNT".localized()
        sigInButton.setTitle("LOG_IN".localized(), for: .normal)
        registrationButton.setTitle("REGISTER".localized(), for: .normal)
        passwordRepeatLabel.text = "REPEAT_PASSWORD".localized()
        passwordLabel.text = "PASSWORD".localized()
        registrationLabel.text = "REGISTER".localized()
        textfieldEmail.placeholder = "ENTER_EMAIL".localized()
        textfieldPassword.placeholder = "ENTER_PASSWORD".localized()
        textfieldRepeatedPassword.placeholder = "REPEAT_PASSWORD".localized()
    }
    
    func hideKeyboardWhenTapped(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    @IBAction func textfieldEditDidBegin(_ sender: TextFieldWithPadding) {
        sender.layer.borderColor = UIColor(red: 0.59, green: 0.33, blue: 0.94, alpha: 1.00).cgColor
    }
    @IBAction func textfieldEditDidEnd(_ sender: TextFieldWithPadding) {
        sender.layer.borderColor = UIColor(red: 0.61, green: 0.64, blue: 0.69, alpha: 1.00).cgColor
    }
    
    @IBAction func showPassword(_ sender: Any) {
        textfieldPassword.isSecureTextEntry = !textfieldPassword.isSecureTextEntry
        
    }
    @IBAction func showRepeatedPassword(_ sender: Any) {
        textfieldRepeatedPassword.isSecureTextEntry = !textfieldRepeatedPassword.isSecureTextEntry
    }

    @IBAction func sigIn(_ sender: Any) {
//        let signIn = storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func sigUp(_ sender: Any) {
        let email = textfieldEmail.text!
        let password = textfieldPassword.text!
        
        SVProgressHUD.show()
        
        let parameters = [
            "email": email,
            "password":password
        ]
        
        AF.request(Urls.SIGN_UP_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseData { [self] response in
            SVProgressHUD.dismiss()
            var resultString = ""
            if let data = response.data {
                resultString = String(data: data, encoding: .utf8)!
                print(resultString)
            }
            
            if response.response?.statusCode == 200 {
//                InvalidInput()
                let json = JSON(response.data!)
                print("JSON: \(json)")
                
                if let token = json["accessToken"].string {
                    Storage.sharedInstance.accessToken = token
                    UserDefaults.standard.set(token, forKey: "accessToken")
                    self.startApp()
                } else {
//                    self.InvalidInput()
                    SVProgressHUD.showError(withStatus: "CONNECTION_ERROR".localized())
                }
            } else {
//                InvalidInput()
                var ErrorString = "CONNECTION_ERROR".localized()
                if let sCode = response.response?.statusCode {
                    ErrorString = ErrorString + " \(sCode)"
                }
                ErrorString = ErrorString + " \(resultString)"
                SVProgressHUD.showError(withStatus: "\(ErrorString)")
            }
        }
    }
    
    func startApp(){
        let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        
        tabBarVC.modalPresentationStyle = .fullScreen
        self.present(tabBarVC, animated: true )
        
    }
    
//    func InvalidInput(){
//        if textfieldPassword.text!.isEmpty || textfieldRepeatedPassword.text!.isEmpty{
//            errorLabel.text = "EMPTY_PASSWORD".localized()
//            errorLabel.isHidden = false
//            }
//        if textfieldPassword.text! != textfieldRepeatedPassword.text!{
//            errorLabel.text = "WRONG_PASSWORD".localized()
//            errorLabel.isHidden = false
//        }else{
//            errorLabel.isHidden = true
//        }
//
//    }
}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


