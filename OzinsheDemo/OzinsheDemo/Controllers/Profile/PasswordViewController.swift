//
//  PasswordViewController.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 01.09.2023.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class PasswordViewController: UIViewController {
    
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    

    @IBOutlet weak var navigationLabel: UINavigationItem!
    

    
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordRepeatLabel: UILabel!
        

    @IBOutlet weak var passwordTextfield: TextFieldWithPadding!
    
    @IBOutlet weak var passwordRepeatedTextfield: TextFieldWithPadding!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        saveButton.layer.cornerRadius = 12
        
        passwordTextfield.layer.cornerRadius = 12.0
        passwordTextfield.layer.borderWidth = 1.0
        passwordTextfield.layer.borderColor = UIColor(red: 0.90, green: 0.92, blue: 0.94, alpha: 1.00).cgColor
        
        passwordRepeatedTextfield.layer.cornerRadius = 12.0
        passwordRepeatedTextfield.layer.borderWidth = 1.0
        passwordRepeatedTextfield.layer.borderColor = UIColor(red: 0.90, green: 0.92, blue: 0.94, alpha: 1.00).cgColor
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureViews()
        hideKeyboardWhenTapped()
    }
    
    
    func languageDidChange() {
        configureViews()
    }
    
    func hideKeyboardWhenTapped(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    
    func configureViews(){
        passwordLabel.text = "PASSWORD".localized()
        passwordRepeatLabel.text = "REPEAT_PASSWORD".localized()
        navigationLabel.title = "PASSWORD_PAGE".localized()
        saveButton.setTitle("SAVE_BUTTON".localized(), for: .normal)
        passwordTextfield.placeholder = "ENTER_PASSWORD".localized()
        passwordRepeatedTextfield.placeholder = "ENTER_PASSWORD".localized()
        
//        for placeHolderText in passwordTextfield{
//            placeHolderText.placeholder = "ENTER_PASSWORD".localized()
//
//        }
    }
    
    
    @IBAction func textEditDidBegin(_ sender: TextFieldWithPadding) {
        sender.layer.borderColor = UIColor(red: 0.59, green: 0.33, blue: 0.94, alpha: 1.00).cgColor
    }
    
    
    @IBAction func textEditDidEnd(_ sender: TextFieldWithPadding) {
        sender.layer.borderColor = UIColor(red: 0.61, green: 0.64, blue: 0.69, alpha: 1.00).cgColor
    }
    
    @IBAction func showPassword(_ sender: Any) {
        passwordTextfield.isSecureTextEntry = !passwordTextfield.isSecureTextEntry
        
    }
    @IBAction func showRepeatedPassword(_ sender: Any) {
        passwordRepeatedTextfield.isSecureTextEntry = !passwordRepeatedTextfield.isSecureTextEntry
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    //MARK: - Change password and save

    @IBAction func saveButtonClicked(_ sender: Any) {
        
        
        
        if passwordTextfield.text! == passwordRepeatedTextfield.text!{
            
            let password = passwordTextfield.text!
            
            let parameters = [
                "password":password
            ]
            
            SVProgressHUD.show()
            
            let headers : HTTPHeaders = ["Authorization": "Bearer \(Storage.sharedInstance.accessToken)"]
            
            AF.request(Urls.CHANGE_PASSWORD, method: .put,parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseData{
                response in
                
                SVProgressHUD.dismiss()
                var resultString = ""
                if let data = response.data{
                    resultString = String(data: data, encoding: .utf8)!
                    print(resultString)
                }
                
                if response.response?.statusCode == 200{
                    let json = JSON(response.data!)
                    print("JSON: \(json)")
                    
                    self.passwordTextfield.text = password
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    var ErrorString = "CONNECTION_ERROR".localized()
                    if let sCode = response.response?.statusCode {
                        ErrorString = ErrorString + " \(sCode)"
                    }
                    ErrorString = ErrorString + " \(resultString)"
                    SVProgressHUD.showError(withStatus: "\(ErrorString)")
                }
            }
        }
        else{
            SVProgressHUD.showError(withStatus: "WRONG_PASSWORD".localized())
        }
    }
}
    

