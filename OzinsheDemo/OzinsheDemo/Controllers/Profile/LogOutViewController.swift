//
//  LogOutViewController.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 28.08.2023.
//

import UIKit

class LogOutViewController: UIViewController, UIGestureRecognizerDelegate, LanguageProtocol {
    
    

    @IBOutlet weak var logoutTableview: UIView!
   
    @IBOutlet weak var exitLabel: UILabel!
    @IBOutlet weak var confirmationLabel: UILabel!
    @IBOutlet weak var yesButton: UIButton!
    
    
    @IBOutlet weak var noButton: UIButton!
    
    var viewTranslation = CGPoint(x: 0, y: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configure()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        tap.delegate = self
        view.addGestureRecognizer(tap)
        
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureViews()
    }
    func languageDidChange() {
        configureViews()
    }
    func configureViews(){
        exitLabel.text = "EXIT".localized()
        yesButton.setTitle("YES_BUTTON".localized(), for: .normal)
        noButton.setTitle("NO_BUTTON".localized(), for: .normal)
        confirmationLabel.text = "CONFIRMATION".localized()
        
    }
    func configure(){
        yesButton.layer.cornerRadius = 12
        
        logoutTableview.layer.cornerRadius = 32
        logoutTableview.clipsToBounds = true
        logoutTableview.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
        
       
    @objc func dismissView(){
        self.dismiss(animated: true)
    }
    
    @objc func handleDismiss(sender: UIPanGestureRecognizer) {
        switch sender.state {
            case .changed:
                viewTranslation = sender.translation(in: view)
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.logoutTableview.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
                })
            case .ended:
                if viewTranslation.y < 100 {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        self.logoutTableview.transform = .identity
                    })
                } else {
                    dismiss(animated: true, completion: nil)
                }
            default:
                break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool{
        if (touch.view?.isDescendant(of: logoutTableview))!{
            return false
        }
            return true
        
    }
    @IBAction func cancel(_ sender: Any) {
        dismissView()
    }
    
    @IBAction func logOut(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "accessToken")
        
        let rootVC = self.storyboard?.instantiateViewController(withIdentifier: "SignInNavigationController") as! UINavigationController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootVC
        appDelegate.window?.makeKeyAndVisible()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
