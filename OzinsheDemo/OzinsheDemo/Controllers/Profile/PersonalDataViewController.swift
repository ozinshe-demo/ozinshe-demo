//
//  PersonalDataViewController.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 01.09.2023.
//
import UIKit
import Localize_Swift
import SVProgressHUD
import SwiftyJSON
import Alamofire

class PersonalDataViewController: UIViewController{
    
    
    @IBOutlet weak var dateTextfield: UITextField!
    
    @IBOutlet weak var navigationLabel: UINavigationItem!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var birthDateLabel: UILabel!
    
    @IBOutlet weak var nameTextfield: UITextField!
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBOutlet weak var saveButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        saveButton.layer.cornerRadius = 12
        loadUserData()
       
        
    }
    
    func loadUserData() {
        SVProgressHUD.show()
        
        let headers: HTTPHeaders = ["Authorization": "Bearer \(Storage.sharedInstance.accessToken)"]
        
        AF.request(Urls.GET_PROFILE, method: .get, headers: headers).responseData { response in
            SVProgressHUD.dismiss()
            
            var resultString = ""
            if let data = response.data{
                resultString = String(data: data, encoding: .utf8)!
                print(resultString)
            }
            
            if response.response?.statusCode == 200{
                let json = JSON(response.data!)
                print("JSON: \(json)")
                
                
                if let email = json["user"]["email"].string {
                    self.emailTextfield.text = email
                }
                if let name = json["name"].string {
                    self.nameTextfield.text = name
                }
                if let phone = json["phoneNumber"].string {
                    self.phoneTextField.text = phone
                }
                if let birthDate = json["birthDate"].string {
                    self.dateTextfield.text = birthDate
                }
            } else {
                SVProgressHUD.showError(withStatus: "Failed")
                
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        configureViews()
        hideKeyboardWhenTapped()
    }
    
    func configureViews(){
        nameLabel.text = "NAME".localized()
        phoneLabel.text = "PHONE".localized()
        birthDateLabel.text = "BIRTHDAY".localized()
        saveButton.setTitle("SAVE_BUTTON".localized(), for: .normal)
        navigationLabel.title = "PERSONAL_INFO".localized()
        
    }
    
    func hideKeyboardWhenTapped(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    //MARK: - Save all changes
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        let name = nameTextfield.text!
        let email = emailTextfield.text!
        let phone = phoneTextField.text!
        
        let language = ""
        let id: Int = 0
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateTextfield.text ?? "") ?? Date()
        let birthDate = dateFormatter.string(from: date)
        
        SVProgressHUD.show()
        
        let parameters: [String: Any] = [
            "birthDate": birthDate,
            "id": id,
            "language": language,
            "name": name,
            "phoneNumber": phone,
            //              "user": [
            //                    "email": email
            //                ]
        ]
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(Storage.sharedInstance.accessToken)"
        ]
        
        AF.request(Urls.UPDATE_PROFILE, method: .put,parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseData{
            response in
            
            SVProgressHUD.dismiss()
            var resultString = ""
            if let data = response.data{
                resultString = String(data: data, encoding: .utf8)!
                print(resultString)
            }
            
            if response.response?.statusCode == 200{
                let json = JSON(response.data!)
                print("JSON: \(json)")
                
                if let email = json["user"]["email"].string {
                    self.emailTextfield.text = email
                }
                self.nameTextfield.text = name
                self.dateTextfield.text = birthDate
                self.phoneTextField.text = phone
                //
                
                self.navigationController?.popViewController(animated: true)
                
            }else {
                var ErrorString = "CONNECTION_ERROR".localized()
                if let sCode = response.response?.statusCode {
                    ErrorString = ErrorString + " \(sCode)"
                }
                ErrorString = ErrorString + " \(resultString)"
                SVProgressHUD.showError(withStatus: "\(ErrorString)")
            }
        }
    }
    
    
    
    
    
}
