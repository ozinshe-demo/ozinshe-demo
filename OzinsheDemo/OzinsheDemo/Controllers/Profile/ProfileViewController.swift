//
//  ProfileViewController.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 24.08.2023.
//

import UIKit
import Localize_Swift
import SVProgressHUD
import SwiftyJSON
import Alamofire

class ProfileViewController: UIViewController, LanguageProtocol {
    
    
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var myProfileLabel: UILabel!
    @IBOutlet weak var languageButton: UIButton!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var navigationLabel: UINavigationItem!
    
    @IBOutlet weak var personalDataButton: UIButton!
    
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var passwordChangeButton: UIButton!
    @IBOutlet weak var termConditionsButton: UIButton!
    @IBOutlet weak var notificationsButton: UIButton!
    
    @IBOutlet weak var darkModeButton: UIButton!
    
    @IBOutlet weak var darkModeSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        notificationSwitch.addTarget(self, action: #selector(notificationChange(paramTarget:)), for: .valueChanged)
        darkModeSwitch.addTarget(self, action: #selector(darkModeisOn(paramTarget:)), for: .valueChanged)
        loadUserData()
  
    }

    
    @objc func darkModeisOn(paramTarget: UISwitch){
        if darkModeSwitch.isOn{
            darkModeSwitch.window?.overrideUserInterfaceStyle = .dark
        }
        else{
            darkModeSwitch.window?.overrideUserInterfaceStyle = .light
        }
    }
    
   
    @objc func notificationChange(paramTarget: UISwitch){
        if notificationSwitch.isOn {
            let alert = UIAlertController(title: "ALERT".localized(), message: "NOTIFICATIONS_ON".localized(), preferredStyle: .alert)
            self.present(alert, animated: true)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                alert.dismiss(animated: true, completion: nil)
            }
        }else{
            let alert = UIAlertController(title: "ALERT".localized(), message: "NOTIFICATIONS_OFF".localized(), preferredStyle: .alert)
            self.present(alert, animated: true)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func termsClicked(_ sender: Any) {
        
        let termsVC = (storyboard?.instantiateViewController(withIdentifier: "termsVC"))! as UIViewController
        navigationController?.show(termsVC, sender: self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureViews()
    }
        
    func configureViews(){
        myProfileLabel.text = "MY_PROFILE".localized()
        languageButton.setTitle("LANGUAGE".localized(), for: .normal)
        personalDataButton.setTitle("PERSONAL_DATA".localized(), for: .normal)
        editLabel.text = "EDIT".localized()
        passwordChangeButton.setTitle("CHANGE_PASSWORD".localized(), for: .normal)
        termConditionsButton.setTitle("TERM_&_CONDITIONS".localized(), for: .normal)
        notificationsButton.setTitle("NOTIFICATIONS".localized(), for: .normal)
        darkModeButton.setTitle("DARK_MODE".localized(), for: .normal)
 
            
        if Localize.currentLanguage() == "en"{
            languageLabel.text = "English"
            navigationLabel.title = "Profile"
        }
        if Localize.currentLanguage() == "kk"{
            languageLabel.text = "Қазақша"
            navigationLabel.title = "Профиль"
        }
        if Localize.currentLanguage() == "ru"{
            languageLabel.text = "Русский"
            navigationLabel.title = "Профиль"
        }
        
    }
    @IBAction func languageShow(_ sender: Any) {
        
        let languageVC = storyboard?.instantiateViewController(withIdentifier: "LanguageViewController") as! LanguageViewController
        
        
        languageVC.modalPresentationStyle = .overFullScreen
        
        languageVC.delegate = self
        
        present(languageVC, animated: true)
    }
    
    @IBAction func logout(_ sender: Any) {
        let logoutProfile = storyboard?.instantiateViewController(withIdentifier: "LogOutViewController") as! LogOutViewController
        logoutProfile.modalPresentationStyle = .overFullScreen
        
        present(logoutProfile, animated: true)
        
    }
    
    
    
    func languageDidChange() {
        configureViews()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func loadUserData() {
        SVProgressHUD.show()
        
        let headers: HTTPHeaders = ["Authorization": "Bearer \(Storage.sharedInstance.accessToken)"]
        
        AF.request(Urls.GET_PROFILE, method: .get, headers: headers).responseData { response in
            SVProgressHUD.dismiss()
            
            var resultString = ""
            if let data = response.data{
                resultString = String(data: data, encoding: .utf8)!
                print(resultString)
            }
            
            if response.response?.statusCode == 200{
                let json = JSON(response.data!)
                print("JSON: \(json)")
                
                
                if let email = json["user"]["email"].string {
                    self.emailLabel.text = email
                }
                
            } else {
                SVProgressHUD.showError(withStatus: "Failed")
                
            }
        }
    }

}
