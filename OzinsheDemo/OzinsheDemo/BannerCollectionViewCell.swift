//
//  BannerCollectionViewCell.swift
//  OzinsheDemo
//
//  Created by Kamila Sultanova on 10.10.2023.
//

import UIKit
import SDWebImage

class BannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImageView: UIImageView!
    
    @IBOutlet weak var categoryBannerLabel: UILabel!
    @IBOutlet weak var categoryView: UIView!
    
    @IBOutlet weak var descriptionBannerLabel: UILabel!
    @IBOutlet weak var titleBannerlabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bannerImageView.layer.cornerRadius = 12.0
        categoryView.layer.cornerRadius = 8.0
        
        
    }
    
    func setData(bannerMovie: BannerMovie){
        
        let transformer = SDImageResizingTransformer(size: CGSize(width: 300, height: 164.0),scaleMode: .aspectFill)
        bannerImageView.sd_setImage(with: URL(string: bannerMovie.link),placeholderImage: nil,context: [.imageTransformer : transformer])
        
        if let categoryName = bannerMovie.movie.categories.first?.name{
            categoryBannerLabel.text = categoryName
        }
        
        titleBannerlabel.text = bannerMovie.movie.name
        descriptionBannerLabel.text = bannerMovie.movie.description

        
    }
}
